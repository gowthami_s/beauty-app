package com.siddi.beautyapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.transition.ChangeBounds;
import android.transition.ChangeImageTransform;
import android.transition.TransitionManager;
import android.transition.TransitionSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    boolean mExpanded = true;
    //boolean mExpanded;
    ViewGroup transitionsContainer;
    ImageView imageView;
    SearchView searchView;

    CardView cardView1, cardView2, cardView3, cardView4, cardView5;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchView = findViewById(R.id.searchView);
        searchView.setQueryHint("Search Products");
        searchView.setBackgroundResource(R.drawable.search_bg_border);

        transitionsContainer = findViewById(R.id.transitions_container);
        imageView = transitionsContainer.findViewById(R.id.image);
        cardView1 = findViewById(R.id.cardview1);
        cardView2 = findViewById(R.id.cardview2);
        cardView3 = findViewById(R.id.cardview3);
        cardView4 = findViewById(R.id.cardview4);
        cardView5 = findViewById(R.id.cardview5);


        //mExpanded = !mExpanded;
        showExpansion(mExpanded);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mExpanded = !mExpanded;

                showExpansion(mExpanded);
            }
        }, 500);


        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "card", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(MainActivity.this, DetailedProductDetails.class);
                startActivity(intent);
                //overridePendingTransition(R.anim.in_animation, R.anim.out_animation);
                overridePendingTransition(R.anim.slide_from_top,R.anim.slide_in_top);
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "card", Toast.LENGTH_LONG).show();

                Intent intent = new Intent(MainActivity.this, DetailedProductDetails.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_animation, R.anim.out_animation);
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "card", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, DetailedProductDetails.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        });

        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "card", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, DetailedProductDetails.class);
                startActivity(intent);
            }
        });

        cardView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainActivity.this, "card", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, DetailedProductDetails.class);
                startActivity(intent);
            }
        });
    }

    @SuppressLint("NewApi")
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void showExpansion(boolean mExpanded) {
        TransitionManager.beginDelayedTransition(transitionsContainer, new TransitionSet()
                .addTransition(new ChangeBounds())
                .addTransition(new ChangeImageTransform()));

        ViewGroup.LayoutParams params = imageView.getLayoutParams();
        params.height = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT;
        imageView.setLayoutParams(params);

        imageView.setScaleType(mExpanded ? ImageView.ScaleType.CENTER_CROP : ImageView.ScaleType.FIT_CENTER);

    }
}
