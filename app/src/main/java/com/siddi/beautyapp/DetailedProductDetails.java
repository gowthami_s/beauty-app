package com.siddi.beautyapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class DetailedProductDetails extends AppCompatActivity {

    TextView add_to_bag_tv;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed_product);

        add_to_bag_tv = findViewById(R.id.add_to_bag_tv);

        add_to_bag_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(DetailedProductDetails.this, "add_to_bag_tv", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(DetailedProductDetails.this, CartDetailProduct.class);
                startActivity(intent);
                overridePendingTransition(R.anim.in_animation, R.anim.out_animation);
            }
        });
    }
}
